Name:    cartographer
Version: 2.0.0
Release: 1
Summary: This is ROS melodic ros_controllers Package
License: Public Domain and Apache-2.0 and BSD and MIT and BSL-1.0 and LGPL-2.1-only and MPL-2.0 and GPL-3.0-only and GPL-2.0-or-later and MPL-1.1 and IJG and Zlib and OFL-1.1
URL:     https://github.com/cartographer-project/cartographer/archive/refs/tags
Source0: https://github.com/cartographer-project/cartographer/archive/refs/tags/2.0.0.tar.gz
BuildRequires:	gcc-c++
BuildRequires:	cmake
BuildRequires:	lz4-devel
BuildRequires:	bzip2-devel
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
BuildRequires:	openssl-devel
BuildRequires:	boost-devel
BuildRequires:	uuid-devel
BuildRequires:	uuid
BuildRequires:	libuuid-devel
BuildRequires:	python3-gpgme
BuildRequires:	gpgme-devel
BuildRequires:	bullet-devel
BuildRequires:	python3-sip-devel
BuildRequires:	protobuf-devel
BuildRequires:	gflags-devel
BuildRequires:	lua-devel
BuildRequires:  gmock-devel
BuildRequires:  cairo-devel
BuildRequires:  sphinx
%description
This is ROS noetic ros_controllers Package.

%prep
%setup

%install
cd 3rdparty/ 

cd empy-3.3.4/
python3 setup.py install --user
cd ..

cd six-1.15.0/
python3 setup.py install --user
cd ..

cd setuptools_scm-4.1.2/
python3 setup.py install --user
cd ..

cd python-dateutil-2.8.1/
python3 setup.py install --user
cd ..

cd pyparsing-2.4.7/
python3 setup.py install --user
cd ..

cd docutils-0.16/
python3 setup.py install --user
cd ..

cd catkin_pkg-0.4.22/
python3 setup.py install --user
cd ..

cd PyYAML-5.3.1/
python3 setup.py install --user
cd ..

cd distro-1.5.0/
python3 setup.py install --user
cd ..

cd rospkg-1.2.8/
python3 setup.py install --user
cd ..

cd tinyxml/
mkdir -p ../../install_isolated/include/tinyxml/
mkdir -p ../../install_isolated/lib/tinyxml/
make
cp tinystr.h ../../install_isolated/include/tinyxml/
cp tinyxml.h ../../install_isolated/include/tinyxml/
cp libtinyxml.so ../../install_isolated/lib/tinyxml/
cd ..

cd ..

./src/catkin/bin/catkin_make_isolated --install

####
# 对install_isoloate内部的变量名称进行替换
#
####
SRC_PATH=$PWD/install_isolated
DST_PATH=/opt/ros/noetic
sed -i "s:${SRC_PATH}:${DST_PATH}:g"  `grep -rIln "${SRC_PATH}" install_isolated/*`

####
# 添加.catkin和.rosinstall文件
#
####
mkdir -p %{buildroot}/opt/ros/noetic/
cp -r install_isolated/* %{buildroot}/opt/ros/noetic/
cp  install_isolated/.rosinstall %{buildroot}/opt/ros/noetic/
cp  install_isolated/.catkin %{buildroot}/opt/ros/noetic/

%files
%defattr(-,root,root)
/opt/ros/noetic/*
/opt/ros/noetic/.rosinstall
/opt/ros/noetic/.catkin

%changelog
* Thu 12-15-2020 openEuler Buildteam <hanhaomin008@126.com>
- Package init